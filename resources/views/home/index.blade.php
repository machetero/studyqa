@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-10 offset-md-1">
            <h1 id="home-header">{{$homePost->title}}</h1>
            <div>{{$homePost->content}}</div>
        </div>
    </div>
</div>
@endsection
