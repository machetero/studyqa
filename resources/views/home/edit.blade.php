@extends('layouts.app')
@section('content')
    <form id="home-edit" method="post" action="{{url('/home/edit')}}">
        {{csrf_field()}}
        <input type="text" name="post_title" placeholder="Title" required><br>
        <textarea name="post_content" placeholder="Content" required></textarea><br>
        <button type="submit">Сохранить</button>
    </form>
@endsection