@extends('layouts.app')
@section('content')
    @foreach($pictures as $picture)
        <a href="/pictures/{{$picture->name}}" class="gallery-item">
            <img width="300" height="300" src="/pictures/{{$picture->name}}" class="img-fluid rounded">
        </a>
    @endforeach
@endsection