@extends('layouts.app')
@section('content')
    <form class="gallery" method="post" action="/gallery" enctype="multipart/form-data">
        {{csrf_field()}}
        <label>Загрузить файл в галлерею
        <input type="file" name="picture">
        </label>
        <button type="submit">Загрузить</button>
    </form>
@endsection