@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-10 offset-md-1">
                <h1>{{$singleNews->title}}</h1>
                {{$singleNews->content}}
            </div>
        </div>
    </div>
@endsection