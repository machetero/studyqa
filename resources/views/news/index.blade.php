@extends('layouts.app')
@section('content')
    <h1 class="news-header">Новости:</h1>
    <div class="container">
        <div class="row">
            @foreach($news as $singleNews)
            <div class="col-4">
                <div class="single-news-wrapper">
                    <h1><a class="single-news-header" href="{{url('news.show',['id'=>$singleNews->id])}}">{{$singleNews->title}}</a></h1>
                    <hr>
                    <div>
                        {{substr($singleNews->content,0, 1000)}}<br>
                        <a href="{{route('news.show',['id'=>$singleNews->id])}}">Дальше >></a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
@endsection