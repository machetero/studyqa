<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomePost extends Model
{
    protected $fillable = ['title','content'];

    protected $table = 'homepost';
}
