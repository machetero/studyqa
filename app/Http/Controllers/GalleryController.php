<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Picture;

class GalleryController extends Controller
{

    public function index()
    {

        $pictures = Picture::all();
        return view('gallery.index',compact('pictures'));

    }

    public function create()
    {

        return view('gallery.create');

    }

    public function store(Request $request)
    {

        if($request->hasFile('picture')) {
            $file = $request->file('picture');
            $extension = $file->getClientOriginalExtension();
            $name = uniqid();
            $name .= '.'.$extension;
            $picture = new Picture(['name' => $name]);
            $picture->save();
            $file->storeAs('/pictures',$name);
            return redirect('gallery');
        }else{
            return response('Выберите картинку для загрузки');
        }
    }

}
