<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HomePost;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $homePost = HomePost::first();

        return view('home.index' , compact('homePost'));

    }

    public function edit()
    {

        return view('home.edit');

    }

    public function store(Request $request)
    {

        $homePost = HomePost::first();
        if(is_null($homePost)){
            $homePost = new HomePost();
        }
        $homePost->title = $request->input('post_title');
        $homePost->content = $request->input('post_content');
        $homePost->save();

        return redirect('/');

    }

}
