<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

//Главная
Route::get('/', 'HomeController@index')->name('home');

Route::get('/home/edit','HomeController@edit')->name('home.edit');

Route::post('/home/edit','HomeController@store')->name('home.store');

//Маршруты новостей
Route::resource('news','NewsController');

//Маршруты галлереи
Route::get('/gallery','GalleryController@index')->name('gallery.index');

Route::get('/gallery/create','GalleryController@create')->name('gallery.create');

Route::post('/gallery','GalleryController@store')->name('gallery.store');

